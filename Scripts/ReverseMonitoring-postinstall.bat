@echo off
rem
rem �������� 䠩� ���⠫��樨 ����ਡ�⨢� � �᭮��묨 䠩���� ����� "Exponenta"
rem
rem USAGE
rem > postinstall.bat
rem ����᪠�� 䠩� � �ࠢ��� �����������

setlocal enableextensions enabledelayedexpansion

Rem ��⠭���� ��⥬����� ��६����� ���㦥���

rem
rem Set Directories Path
set curdirforurl=%CD%
set UTIL=c:\Util
set DEST_DIR=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set LIBBATPUB=c:\pub1\Distrib\LIB\BAT
set pathCMD=%SystemRoot%\System32

set Dest_DIR=c:\pub1

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\CURL.EXE
set WGETEXE=%UTIL%\wget.exe

set Program Variables
rem
set CSCRIPTEXE=%pathCMD%\cscript.exe
set REGEXE=%pathCMD%\reg.exe
set INSTALL_STCMD=%LIBBATPUB%\Install.St.cmd

rem Check Integrity
rem
echo Check Integrity...
if not exist %CURLEXE% exit /b 0
if not exist %WGETEXE% exit /b 0
if not exist %CSCRIPTEXE% exit /b 0
if not exist %REGEXE% exit /b 0
rem if not exist %INSTALL_STCMD% exit /b 0

rem Add ExecutionPolicy
rem
echo Add Execution Policy...
%REGEXE% add "HKLM\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" /v ExecutionPolicy /t REG_SZ /d "Unrestricted" /f
%REGEXE% add "HKCU\SOFTWARE\Microsoft\PowerShell\1\ShellIds\Microsoft.PowerShell" /v ExecutionPolicy /t REG_SZ /d "Unrestricted" /f

echo -
echo Welcome to RemoteMonitoring Installer!!
echo This program install the packet into directory %Dest_DIR%
echo -

rem Write Config Files 
rem @echo on
echo "=== Changing Exponente Config ==="
echo -

Rem Change Execution Code...

%SystemRoot%\System32\cscript.exe //NoLogo %Dest_DIR%\Util\ReverseMonitoring-SearchReplace.vbs

Rem Execute Changed Script

call %Dest_DIR%\Util\ReverseMonitoring-postinstall1.bat

echo Installation is made with Success!
rem
rem ��ࠢ�� ���� ��⠭���� �� �ࢥ� 宧鶴�
rem pause
echo Send Logs...
if exist %INSTALL_STCMD% call %INSTALL_STCMD%
exit /b 0
